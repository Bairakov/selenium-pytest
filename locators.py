
from selenium.webdriver.common.by import By

class ClientLocators():
    BUTTON_ENTRANCE_BY_EMAIL = (By.XPATH,"(//button[@class='btn btn-outline-primary'])[2]" )
    EMAIL = (By. XPATH, "//input[@id = 'email-input']")
    PASSWORD = (By.XPATH, "//input[@id = 'password-input']")
    BUTTON_COME_IN = (By.XPATH, "//button[contains(@class,'btn btn-lg')]") #вход
    MODAL_WINDOW = (By.XPATH, "//button[@class='tenchat-dialog-btn tenchat-dialog-btn--close']")#модальное окно
    MY_COMPANY = (By.XPATH, "//div[@class='cl-nav-menu-link']")
    LEADERS_AND_FOUNDERS = (By.XPATH, "(//span[@class='cl-nav-sub-content'])[2]") #руководители и учредители
    LEGAL_LEADERS_AND_FOUNDERS = (By.XPATH, "(//a[@role='tab'])[2]") #юридические лица
    INDIVIDUAL_LEADERS_AND_FOUNDERS =(By.XPATH, "//a[@role='tab']") #физические лица
    BANK_REQUISITES = (By.XPATH, "//span[text()=' Банковские реквизиты ']")# банковские реквизиты 
    ADD_REQUISITES_BUTTON = (By.XPATH, "//div[@class='d-flex justify-content-center']//button[1]")# добавить реквизиты
    SPINNER = (By.CSS_SELECTOR, "#spinner")

class RegistationLocators():
    INN_COMPANY = (By.XPATH, "//input[@name = 'inn']")
    FIO = (By.XPATH, "//input[@name= 'fullName']")
    PHONE = (By.XPATH, "//input[@name= 'phone']")
    EMAIL = (By.XPATH, "//input[@name= 'email']")
    PRIVACY_POLICY = (By.XPATH, "//label[@for='privacy-policy']")
    BUTTON_REG = (By.XPATH, "//button[text()=' Зарегистрироваться ']")
    MAIL_LOGIN = (By.XPATH, "//input[@name = '_user']")
    MAIL_PASSWORD = (By.XPATH, "//input[@name = '_pass']")
    BUTTON_MAIL_ENTRY = (By.XPATH, "//button[@id= 'rcmloginsubmit']")
    
    ACTIVE_PHONE = (By.XPATH, "//input[@name ='phone']")
    ACTIVE_PASSWORD = (By.XPATH, "//input[@name ='password']")
    CONF_PASSWORD = (By.XPATH, "//input[@name ='confirmPassword']")
    BUTTON_ACTIVE = (By.XPATH, "//button[contains(text(),'Под')]")
    ALLERT = (By.XPATH, "//div[text()=' Компания ликвидирована ']")


import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options as FirefoxOptions

def pytest_addoption(parser):
    parser.addoption('--browser_name', action='store', default='chrome',
                     help="Choose browser: chrome or firefox")
    parser.addoption('--language', action='store', default='es',
                     help="Choose lang")


@pytest.fixture(scope="function" )
def browser(request):
    browser_name = request.config.getoption("browser_name")
    
    user_language = request.config.getoption("language")
    if browser_name == "chrome":
        print("\nstart chrome browser for test..")
        
        options = Options()
        options.add_experimental_option('prefs', {'intl.accept_languages': user_language})
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('window-size=1920,1080')
        options.add_extension("extension_1_2_8_0.crx")
        #options.add_argument('headless')
        browser = webdriver.Chrome(options=options)
        
       
        
        
    elif browser_name == "firefox":
        print("\nstart firefox browser for test..")
    
        options = FirefoxOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        #options.add_argument('window-size=1920,1080')
        #options.add_extension("extension_1_2_8_0.crx")
        #options.add_argument("--headless")
        browser = webdriver.Firefox(options=options, executable_path = '/home/test/autotest/geckodriver')
        extension_path = "/home/test/autotest/firefox_cryptopro_extension_latest.xpi"
        browser.install_addon(extension_path, temporary=True)
        browser.set_window_size(1920,1080)
        
        
    else:
        print("Browser <browser_name> still is not implemented")
    yield browser
    print("\nquit browser..")
    browser.quit()

import time
from selenium.webdriver.common.by import By
from .base_page import BasePage
from .locators import ClientLocators
from .locators import RegistationLocators
import psycopg2
import functools
import random
from faker import Faker
from faker import Factory
from pynput.keyboard import Key, Controller
from selenium.webdriver.common.keys import Keys
import os
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC




directory = "/home/test/autotest"
file_name = "test123.txt"
file_path = os.path.join(directory, file_name)





class Client(BasePage):
    def client_registration(self):
        company_database = psycopg2.connect(dbname='test_nalog', 
            user='testuser', 
            password= "testpassuser", 
            host='192.168.2.149',
            ) #connect to the DB

        cur = company_database.cursor() 

        cur.execute(
            """SELECT inn 
    FROM data_mart.t_egrul_mart 
    WHERE opf_name LIKE  'Общества%'
    and term_way_name IS NULL and pfr_reg_number is not null and random()  < 0.001 limit 1;  
            """
            )

        cheakinn = cur.fetchone()
        print (cheakinn)




        company_database = psycopg2.connect(dbname='vbc', 
            user='postgres', 
            password= "12345", 
            host='192.168.1.241',
            ) #connect to the DB

        cur = company_database.cursor()  


        cheakinn
        cur.execute(
        """
        SELECT inn 
        FROM public.vbc_client
        WHERE inn = %s;
        """,
        [cheakinn,]
    )
        result = cur.fetchone()

        if result == None:
            print('нет совпадений')
            global reg_inn
            reg_inn = cheakinn
        elif result == cheakinn:
            print('инн занят')

        a = functools.partial(random.randint, 0, 9)
        gen = lambda: "test3{}{}{}@vbc.loc".format(a(), a(), a(), a(), a(), a(), a(), a(), a(), a(), a())
        gen()
        num = gen()

        

        cur.execute(
            """
            SELECT email
            FROM public.vbc_client_contacts
            WHERE email = %s;
            """,
            [num]
    
        )
        result1 = cur.fetchone()
        


        while result1 is not None:
            company_database = psycopg2.connect(dbname='vbc', 
                user='postgres', 
                password= "12345", 
                host='192.168.1.241',
                ) #connect to the DB

            cur = company_database.cursor() 

            a = functools.partial(random.randint, 0, 9)
            gen = lambda: "test3{}{}{}@vbc.loc".format(a(), a(), a())
            gen()
            num = gen()

            print(num)

            cur.execute(
                """
                SELECT email
                FROM public.vbc_client_contacts
                WHERE email = %s;
                """,
                [num]
                
            )
            result1 = cur.fetchone()
            
        else:
            global cheakEmail
            cheakEmail = num
            print (cheakEmail)
        
    
        company_database = psycopg2.connect(dbname='test_zakupki', 
            user='testuser', 
            password= "testpassuser", 
            host='192.168.2.149',
            ) #connect to the DB

        cur = company_database.cursor() 

        cur.execute(
            """select purchase_number from public.t_notifications
where law = '44' and bidding_date is not null and random() < 0.001  limit 1; 
            """
        )
        global zakupka
        zakupka = cur.fetchone()
        print (zakupka)




        myGenerator = Factory.create('ru_RU')

        reg_inn1 = self.browser.find_element(*RegistationLocators.INN_COMPANY)
        reg_inn1.send_keys(reg_inn)
        fio = self.browser.find_element(*RegistationLocators.FIO)
        fio.send_keys(myGenerator.name())
        phone = self.browser.find_element(*RegistationLocators.PHONE)
        phone.send_keys("0000000000") 
        email = self.browser.find_element(*RegistationLocators.EMAIL)
        email.send_keys(cheakEmail)
        privacy = self.browser.find_element(*RegistationLocators.PRIVACY_POLICY)
        privacy.click()
        button = self.browser.find_element(*RegistationLocators.BUTTON_REG)
        button.click()
        assert self.is_not_element_present(*RegistationLocators.ALLERT), "Компания ликвидирована"

        time.sleep(2)



    def client_confirm_mail(self):

        login = self.browser.find_element(*RegistationLocators.MAIL_LOGIN)
        login.send_keys("test1@vbc.loc")
        password = self.browser.find_element(*RegistationLocators.MAIL_PASSWORD)
        password.send_keys("testpassuser")
        button = self.browser.find_element(*RegistationLocators.BUTTON_MAIL_ENTRY)
        button.click()
        xpath_give = "//span[text()='" + cheakEmail + "']"  
        while self.check_exists_by_xpath(self.browser,"//span[text()='" + cheakEmail + "']") is False:
            self.browser.refresh()
        if self.check_exists_by_xpath(self.browser,"//span[text()='" + cheakEmail + "']") is True:
            self.do_click_by_xpath(self.browser, xpath_give)
            self.do_click_by_xpath(self.browser, xpath_give)
        
        activate_account = self.browser.find_element(By.XPATH, "//a[contains(text(),'test')][@rel = 'noreferrer']")
        activate_account.click()
        
        new_window = self.browser.window_handles[1]
        self.browser.switch_to.window(new_window)
        phone = self.browser.find_element(*RegistationLocators.ACTIVE_PHONE)
        phone.send_keys("0000000000")
        password = self.browser.find_element(*RegistationLocators.ACTIVE_PASSWORD)
        password.send_keys("123")
        password_confirm = self.browser.find_element(*RegistationLocators.CONF_PASSWORD)
        password_confirm.send_keys("123")
        button = self.browser.find_element(*RegistationLocators.BUTTON_ACTIVE)
        button.click()


    def leaders_and_founders_filling(self):

        
        my_company = self.browser.find_element(*ClientLocators.MY_COMPANY)
        my_company.click()
        leaders_and_founders = self.browser.find_element(*ClientLocators.LEADERS_AND_FOUNDERS)
        leaders_and_founders.click()
        legal = self.browser.find_element(*ClientLocators.LEGAL_LEADERS_AND_FOUNDERS)
        legal.click()

        xpath_give = "(//button[contains(@class,'dropdown-toggle btn')])[1]"  # удалить юр лицо если есть
        while self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)
            self.do_click_by_xpath(self.browser, "/html/body/div[2]/div/button[2]")
            self.do_click_by_xpath(self.browser, "//button[text()='Да']")
            self.browser.refresh()
            legal = self.browser.find_element(*ClientLocators.LEGAL_LEADERS_AND_FOUNDERS)
            legal.click()

        individual = self.browser.find_element(*ClientLocators.INDIVIDUAL_LEADERS_AND_FOUNDERS)
        individual.click()

        xpath_give = "(//button[contains(@class,'dropdown-toggle btn')])[2]"  # удалить учредителя
        while self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)
            self.do_click_by_xpath(self.browser, "/html/body/div[2]/div/button[2]")
            self.do_click_by_xpath(self.browser, "//button[text()='Да']")
            self.browser.refresh()

        xpath_give = "//button[text()='Действие']"
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)
            self.do_click_by_xpath(self.browser, "//button[text()='Открыть']")
        elif self.check_exists_by_xpath(self.browser,"//u[text()='+ Добавить']") is True:
            self.do_click_by_xpath(self.browser, "//u[text()='+ Добавить']")
            surname = self.browser.find_element(By.XPATH, "//input[@formcontrolname='surname']")
            surname.send_keys("Облонский")
            name = self.browser.find_element(By.XPATH, "//input[@formcontrolname='name']")
            name.send_keys("Степан")
            
        inn = self.browser.find_element(By.XPATH, "//input[@formcontrolname='inn']")
        inn.clear()
        inn = self.browser.find_element(By.XPATH, "//input[@formcontrolname='inn']")
        inn.send_keys("213213213213")
        role_in_company = self.do_click(self.browser,"/html/body/ngb-modal-window/div/div/vbc-company-person/div[2]/ul/li[2]") # роль в организации 
        
        element = self.do_click(self.browser,"//label[@for='founderTrue']") # Акционер с долей не менее 1%: да
        element = self.do_click(self.browser,"//label[@for='beneficTrue']") #Бенефициар / Доля в УК (%): да
        element = self.browser.find_element(By.XPATH, "//input[@formcontrolname='percent']").clear() # доля в уставном капитале стереть 
        
        element = self.browser.find_element(By.XPATH,"//input[@formcontrolname='percent']") # доля в уставном капитале вписать 100
        element.send_keys("100")
        element = self.do_click(self.browser,"//label[@for='ceoTrue']") # Единоличный исп. орган: да
        element = self.browser.find_element(By.XPATH, "//input[@formcontrolname='position']").clear() # удалить поле директор
        
        element = self.do_send(self.browser,"//input[@formcontrolname='position']",'директор') # директор
        element = self.browser.find_element(By.XPATH, "//input[@formcontrolname='datePosition']").clear() #удлаить дату 
        
        element = self.do_send(self.browser,"//input[@formcontrolname='datePosition']", '31.01.2018') # ввести дату 
        element = self.do_click(self.browser,"/html/body/ngb-modal-window/div/div/vbc-company-person/div[2]/ul/li[3]") # перейти в паспортные данные
        element = self.do_send(self.browser,"//input[@formcontrolname='issuedWhen']",'31.01.2018') # дата выдачи
        element = self.do_send(self.browser,"//input[@formcontrolname='subcode']", '123-456') # код подразделения
        element = self.browser.find_element(By.XPATH, "(//div[@role='combobox']//input)[2]") # кем выдан
        element.send_keys("умвд по городу москве") # ввести 
        
        element.send_keys(Keys.ENTER) # нажать энтер
        element = self.do_send(self.browser,"//input[@formcontrolname='series']", '1231')# серия
        element = self.do_send(self.browser,"//input[@formcontrolname='number']", '123123')# номер
        element = self.do_click(self.browser,"//label[@for='genderMalePassport']") # муж пол
        element = self.do_send(self.browser,"//input[@formcontrolname='birthday']",'23.01.1975') # дата рождения 
        element = self.do_send(self.browser,"//input[@formcontrolname='placeBirth']", 'Москва') # место рождения 
        element = self.do_click(self.browser,"//button[text()=' Загрузить ']") # загрузить файл
        
        element = self.browser.find_element(By.XPATH, "/html/body/input") # загрузить файл
        element.send_keys(file_path) # загрузить файл
        element = self.do_click(self.browser,"/html/body/ngb-modal-window/div/div/vbc-company-person/div[2]/ul/li[4]") # адресс регистрации 
        element = self.do_click(self.browser,"(//div[@role='combobox']//input)[3]") # регион
        element = self.do_click(self.browser,"//div[@role='option']") # москва
        time.sleep(2)
        element = self.browser.find_element(By.XPATH, "//*[@id='street']/div/div/div[2]/input") # улица
        element.send_keys("ленина") # ввести 
        element = self.do_click(self.browser,"(//span[text()='ул Ленина'])[2]")
        
        element = self.do_send(self.browser,"//*[@id='house']/div/div/div[2]/input",'1') # дом
        
        element = self.do_click(self.browser,"(//div[@role='option']/following-sibling::div)[3]")
        #element.send_keys(Keys.ENTER) # нажать энтер
        element = self.do_click(self.browser,"//button[text()=' Сохранить ']")

    
    def bank_requisites_and_115(self):
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        self.do_click(self.browser,"//span[text()=' Банковские реквизиты ']") # банковские реквизиты 
        self.do_click(self.browser,"(//button[@type='button']//span)[2]") # добавить счет
        self.do_send(self.browser,"//input[@id = 'bankBic']",'044525823')
        self.do_send(self.browser,"//input[@id = 'checkAccount']",'00000810123234231234')
        self.do_click(self.browser,"(//button[contains(@class,'btn btn-primary')])[2]")
        self.do_click(self.browser,"//vbc-cl-nav-sub[@routerlink='company/form-115']") #115 фз
        
        self.do_click(self.browser,"//button[contains(@class,'btn btn-sm')]") # сохранить

    
    
    def get_bg(self):
        a = '1234567890'
        global number_order
        number_order = ''
        global string_number_order
        self.do_click(self.browser,"//span[text()='Получить продукт']") # получаем продукт 
        self.do_click(self.browser,"//div[@class='credit-header-subtitle mb-2']/following-sibling::div[1]") # выбираем БГ 
        self.do_click(self.browser,"(//div[@routerlink='/client/bg-light/order/new']//div)[2]")
        self.do_send(self.browser,"//input[@formcontrolname='purchaseNumber']",zakupka)
        self.do_click(self.browser,"//button[@class='mt-3 success']")
        time.sleep(1)
        element = self.browser.find_element(By.XPATH, "//div[@role='combobox']").click()
        element = self.browser.find_element(By.XPATH, "//span[text()='Исполнение контракта']").click()
        self.browser.find_element(By.XPATH, "//input[@formcontrolname='amount']").clear()
        self.do_send(self.browser,"//input[@formcontrolname='amount']", '458 000') # сумма бг
        self.do_send(self.browser,"//input[contains(@class,'ng-empty ng-untouched')]",'09.06.2022')
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        self.do_click(self.browser,"//button[text()=' Далее ']")
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        self.do_click(self.browser,"//button[text()=' Далее ']")
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        xpath_give = "//span[text()='Загрузить']"  # загрузить файл
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)
            element = self.browser.find_element(By.XPATH, "/html/body/input") # загрузить файл
            element.send_keys(file_path)
        
        string_number_order = self.browser.find_element(By.XPATH, "(//div[@class = 'item'])[1]").text
        for i in string_number_order:
            if i in a:
                number_order = number_order + i

        
        print(string_number_order)
        time.sleep(3)
        self.do_click(self.browser, "//button[text()=' Запустить проверку ']")
        time.sleep(5)


    def client_login(self):
        button = self.browser.find_element(*ClientLocators.BUTTON_ENTRANCE_BY_EMAIL)
        button.click()
        email_field = self.browser.find_element(*ClientLocators.EMAIL)
        email_field.send_keys(cheakEmail)
        password_field = self.browser.find_element(*ClientLocators.PASSWORD)
        password_field.send_keys("123") 
        entry = self.browser.find_element(*ClientLocators.BUTTON_COME_IN)
        entry.click()


    def client_subscribe(self):
        xpath_give = "//button[contains(@class,'tenchat-dialog-btn tenchat-dialog-btn--close')]"  # Закрыть всплывающее окно
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)
        self.browser.refresh()
        self.do_click(self.browser,"(//vbc-cl-nav-menu-name[@class='cl-nav-menu-name']//span)[3]")
        self.do_click(self.browser,"//span[@class='cl-nav-sub-content']//span[1]")
        self.do_click(self.browser,"//td[contains(text(),'" + number_order + "')]")
        time.sleep(1)
        self.do_click(self.browser,"//span[text()='Перейти в заявку']")
        
        
        self.do_click(self.browser,"//button[@theme='success']")
        self.do_click(self.browser,"//button[text()='Подписать и отправить']")
        time.sleep(1)
        self.browser.find_element(By.XPATH, "//div[@role='combobox']").click()
        self.browser.find_element(By.XPATH, "//div[text()='of-test-python-hand-01']").click()


        time.sleep(7)  
        
        


    def client_accept_offer(self):
        xpath_give = "//button[contains(@class,'tenchat-dialog-btn tenchat-dialog-btn--close')]"  # Закрыть всплывающее окно
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)
        self.browser.refresh()
        self.do_click(self.browser, "(//vbc-cl-nav-menu-name[@class='cl-nav-menu-name']//span)[3]") # продукты по бизнесу
        self.do_click(self.browser, "//span[@class='cl-nav-sub-content']//span[1]") # БГ
        self.do_click(self.browser, "//div[contains(text(),'Е')]")#Есть предложение
        time.sleep(2)
        xpath_give = "//vbc-offer-preview[@prefix='BG']" # Закрыть всплывающее окно
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, "//td[@class = 'text-center'][1]")
        
        self.do_click(self.browser,"//td[contains(text(),'" + number_order + "')]") # кликнуть по заявке 
        self.do_click(self.browser,"//span[text()='Перейти в заявку']") # перейти в заявку
        
        self.do_click(self.browser,"//td[text()=' Открыть заявку ']")# перейти 
        self.do_click(self.browser,"//button[text()='Принять предложение']")# принять 
        self.do_click(self.browser,"//button[text()=' Самовывоз ']") # самовывоз
        self.do_click(self.browser,"//label")
        
        self.do_click(self.browser,"//button[text()=' Принять и подписать предложение ']")
        
        self.browser.find_element(By.XPATH,"//div[text()='1 подпись в наличии']/following-sibling::div").click()
        self.browser.find_element(By.XPATH,"//div[text()='of-test-python-hand-01']").click()
        self.browser.find_element(By.XPATH,"//button[@type='button']").click()




        time.sleep(10)


    def get_chrome(self):
        self.do_send(self.browser,'//*[@id="new_node_name"]',"https://test.vbc.loc")
        time.sleep(1)
        self.do_click(self.browser,'//*[@id="add_button"]')
        time.sleep(1)
        self.do_click(self.browser,'//*[@id="btn_save"]')


class Oper(BasePage):
    def oper_login(self):
        self.do_click(self.browser,"//button[@class='btn btn-outline-primary']")
        self.do_send(self.browser,"//input[@id = 'email-input']",'oper@email.ru') #вводим логин 
        self.do_send(self.browser,"//input[@id = 'password-input']",'123') #ввод пароля
        self.do_click(self.browser,"//button[contains(@class,'btn btn-lg')]") #войти 
        
        
    def oper_scoring(self):
        xpath_give = "//button[contains(@class,'tenchat-dialog-btn tenchat-dialog-btn--close')]"  # Закрыть всплывающее окно
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)
        self.do_click(self.browser, "//span[text()='Банковские Гарантии']")
        self.do_click(self.browser, "(//span[text()=' Заявки '])[2]")
        time.sleep(2)
        element = self.browser.find_element(By.XPATH, "//td[contains(text(),'" + number_order + "')]")
        actions = ActionChains(self.browser)
        actions.double_click(element).perform()
        
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        self.do_click(self.browser,"//button[@theme='primary']") # взять на валидацию
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        self.do_click(self.browser,"(//button[@vbcuibutton])[2]") # далее

        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        elements = self.browser.find_elements(By.XPATH,"//button[text()=' Подтвердить ']")
        for element in elements:
            element.click()
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        #time.sleep(2)
        self.do_click(self.browser,"//button[text()=' Подтвердить и сохранить ']")
        #time.sleep(1)

        self.do_click(self.browser,"(//button[@class='light ng-star-inserted'])[2]") #далее
        
        #time.sleep(2)
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        elements = self.browser.find_elements(By.XPATH,"//div[contains(@class,'col-auto p-2 border-white border-right align-items-center d-flex ng-star-inserted')]")
        for element in elements:
            element.click()
            #time.sleep(0.05)
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))

        
        self.do_click(self.browser, "//button[text()=' Сохранить ']")
        self.do_click(self.browser, "//span[text()='Далее']") # далее 
        xpath_give = "//button[text()=' Внести данные ']"  # ввести данные если есть
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)
            self.do_click_by_xpath(self.browser, "(//input[@formcontrolname='noInvalidityData'])[2]")
            self.do_click_by_xpath(self.browser, "//button[text()=' Отправить ']")
        
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        #time.sleep(2)
        self.do_click(self.browser, "(//input[@name='ngb-radio-7']/following-sibling::span)[2]") #Участие акционеров...>нет
        self.do_click(self.browser, "//button[text()=' Сохранить и продолжить ']")#Сохранить донесение информации
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        self.do_click(self.browser, "//span[text()='Далее']")#Далее
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        #time.sleep(3)
        agreed = self.browser.find_element(By.XPATH,"/html/body/div[1]/div[3]/div/vbc-partial-order-viewer/div[2]/vbc-bg-order-result/vbc-partial-order-viewer-body/vbc-ui-toggle[2]/div[2]/vbc-op-partner-list/table/tbody/tr[1]/td[1]/vbc-checkbox/input")
        if agreed.get_attribute("checked") != "true":
            self.browser.find_element(By.XPATH,"(//td[@rowspan='1']//vbc-checkbox)[1]").click()
    #element = def_func.wait_do_click_list(driver, "(//td[@rowspan='1']//vbc-checkbox)")#Произвести клики по всем банкам
        self.do_click(self.browser,"//label[@for='not_purchase_executed']") #закупка не сотоялась
        self.do_click(self.browser, "//button[@theme='success']")

    
        time.sleep(2)





    
class Bank(BasePage):
    def bank_login(self):
        self.do_click(self.browser,"//button[@class='btn btn-outline-primary']")
        self.do_send(self.browser,"//input[@id = 'email-input']",'gpb@bank.ru') #вводим логин 
        self.do_send(self.browser,"//input[@id = 'password-input']",'123') #ввод пароля
        self.do_click(self.browser,"//button[contains(@class,'btn btn-lg')]") #войти 


    def bank_send_offer(self):
        xpath_give = "//button[contains(@class,'tenchat-dialog-btn tenchat-dialog-btn--close')]"  # Закрыть всплывающее окно
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)

        self.do_click(self.browser,"(//vbc-pr-nav-menu-name[@class='pr-nav-menu-name']//span)[2]") # БГ экспресс
        self.do_click(self.browser,"//vbc-pr-nav-sub[@routerlink='bg/list/express']//span[1]") #заявки
        time.sleep(2)
        element = self.browser.find_element(By.XPATH, "//div[contains(text(),'" + number_order + "')]") # открываем заявку 
        actions = ActionChains(self.browser)
        actions.double_click(element).perform() # кликаем по ней 

        self.do_click(self.browser,"//div[contains(@class,'direct-children-ml-1 col')]//button") #взять на рассмотрение 
        self.do_click(self.browser,"//button[text()='Да']") # да 
        self.do_click(self.browser,"//button[text()=' Отправить на ЭКК ']") # отправить на ЭКК
        self.do_click(self.browser,"//button[text()='Да']") # да 
        self.do_click(self.browser,"//button[text()=' Сформировать  предложение ']") # сформировать предложение 
        self.do_click(self.browser,"//button[text()='Да']") # да 
        WebDriverWait(self.browser, 20).until(EC.invisibility_of_element((By.CSS_SELECTOR, "#spinner")))
        self.do_click(self.browser,"//button[text()=' Отправить предложение ']") # отправить предложение 
        time.sleep(5)

    def bank_confirm_payment_and_issue_bg(self):
        xpath_give = "//button[contains(@class,'tenchat-dialog-btn tenchat-dialog-btn--close')]"  # Закрыть всплывающее окно
        if self.check_exists_by_xpath(self.browser, xpath_give) is True:
            self.do_click_by_xpath(self.browser, xpath_give)

        self.do_click(self.browser,"(//vbc-pr-nav-menu-name[@class='pr-nav-menu-name']//span)[2]") # БГ экспресс
        self.do_click(self.browser,"//vbc-pr-nav-sub[@routerlink='bg/list/express']//span[1]") #заявки
        self.do_click(self.browser,"//label[text()[normalize-space()='Все заявки']]") # все заявки 
        time.sleep(2)
        element = self.browser.find_element(By.XPATH, "//div[contains(text(),'" + number_order + "')]") # открываем заявку 
        actions = ActionChains(self.browser)
        actions.double_click(element).perform() # кликаем по ней 
        value = self.browser.find_element(By.XPATH,"//div[text()='Сумма комиссии:']/following-sibling::div").text
        element = self.do_send(self.browser,"//input[@formcontrolname='orderId']",number_order) # ввод номера заявка
        element = self.do_send(self.browser,"//input[@formcontrolname='amount']",value) # ввод суммы комиссии 
        element = self.do_click(self.browser,"(//button[contains(@class,'btn btn-lg')])[3]") # подтвердить платеж 
        element = self.do_click(self.browser,"//button[text()='Да']")# да
        time.sleep(2)
        
        elementScroll = self.browser.find_element(By.XPATH,"(//button[text()=' Действия '])[3]") # поиск элемента 
        self.browser.execute_script("arguments[0].scrollIntoView(true);", elementScroll) # скролл к элементу 
        element = self.do_click(self.browser,"(//button[contains(@class,'dropdown-toggle btn')])[3]") #действия 
        element = self.browser.find_element(By.XPATH,"//div[@class='dropdown-menu show']//button[1]").click() #загрузить 
        element = self.browser.find_element(By.XPATH,"/html/body/input")
        element.send_keys(file_path)
        time.sleep(2)
        element = self.do_click(self.browser,"//button[text()=' Выпустить банковскую гарантию ']") # выпустить БГ 
        time.sleep(2)
        element = self.do_click(self.browser,"//button[text()='Да']")# да


        time.sleep(3)

class WriteToFile():
    def write_to_file(self):


        print('БГ '+ string_number_order + ' выдана')
        new_file = open('order_numberOOO.txt', 'a')
        list_of_tuples = '\n',reg_inn,',',number_order,',', cheakEmail,',', zakupka
        f = open('order_numberOOO.txt', 'a')
        for t in list_of_tuples:
            line = ''.join(str(x) for x in t)
            
            f.write(line + ' ')
        f.close()


from .pages.class_page import Client
from .pages.class_page import Oper
from .pages.class_page import Bank
from .pages.class_page import WriteToFile
import time
import pytest
import testit

@testit.externalID('0002')
@testit.displayName('selenium - pytest autotest')	



@pytest.mark.dependency
def test_reg(browser):
    link = "https://test.vbc.loc/resources/secured/app/auth/sign-up/by-email"
    page = Client(browser, link)
    page.open()
    page.client_registration()
        
@pytest.mark.dependency(depends=['test_reg'])
def test_reg_and_get_Bg(browser):
    link = "http://mail.vbc.loc/?_task=mail&_mbox=INBOX"
    page = Client(browser, link)
    page.open()
    page.client_confirm_mail()
    page.leaders_and_founders_filling()
    page.bank_requisites_and_115()
    page.get_bg()
        
        

    
        
@pytest.mark.dependency(depends=['test_reg','test_reg_and_get_Bg'])
def test_oper_login_and_scoring(browser):
    link = "https://test.vbc.loc/resources/secured/app/entry/login/oper-or-partner"
    page = Oper(browser, link)
    page.open()
    page.oper_login()
    page.oper_scoring()

@pytest.mark.dependency(depends=['test_reg','test_reg_and_get_Bg'])
def test_client_subscribe(browser):
    link = "https://test.vbc.loc/resources/secured/app/entry/login/client"
    page = Client(browser, link)
    page.open()
    page.client_login()
    page.client_subscribe()


@pytest.mark.dependency(depends=['test_reg','test_reg_and_get_Bg'])
def test_bank_send_offer(browser):
    link = "https://test.vbc.loc/resources/secured/app/entry/login/oper-or-partner"
    page = Bank(browser, link)
    page.open()
    page.bank_login()
    page.bank_send_offer()


@pytest.mark.dependency(depends=['test_reg','test_reg_and_get_Bg'])
def test_client_accept_offer(browser):
    link = "https://test.vbc.loc/resources/secured/app/entry/login/client"
    page = Client(browser, link)
    page.open()
    page.client_login()
    page.client_accept_offer()

@pytest.mark.dependency(depends=['test_reg','test_reg_and_get_Bg'])
def test_bank_confirm_payment_and_issue_bg(browser):
    link = "https://test.vbc.loc/resources/secured/app/entry/login/oper-or-partner"
    page = Bank(browser, link)
    page.open()
    page.bank_login()
    page.bank_confirm_payment_and_issue_bg()



@pytest.mark.dependency(depends=['test_reg'])
def test_write_to_file():
    page = WriteToFile()
    page.write_to_file()
